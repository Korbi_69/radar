//
// Created by hsa on 1/20/18.
//

#include "ars_408.hpp"

ars_408::ars_408(uint8_t sensorId, ros::NodeHandle tx_node_handler)
: server(io_service, std::bind(&ars_408::CanDataReceived, this, std::placeholders::_1), RadarRxPort, RadarTxPort)
{
    sensorId = sensorId;
    ros::NodeHandle cfg_handle;
    void* radar_config_msg;

    ars_publisher = tx_node_handler.advertise<ots_ars408::ars408_ObjectList>("ars408_objectList", 256);

    cout << "Starting Sensor " << (int) sensorId << endl;
    try {
        //boost::asio::io_service io_service;
        //peak server(io_service, std::bind(&ars_408::CanDataReceived, this, std::placeholders::_1), RadarRxPort, RadarTxPort);
        //server=&peak(io_service, std::bind(&ars_408::CanDataReceived, this, std::placeholders::_1), RadarRxPort, RadarTxPort);
        config_radar();

        uint64_t swapHelper;
        memcpy(&swapHelper, &ars408_cfg0x200, sizeof(ars408_Cfg0x200_t));

        radar_config_msg = (void*)Swap8Bytes(swapHelper);
        t = boost::thread(&ars_408::rosSubscriber, this);
        server.peakTx(radar_config_msg, 8, 0x200);

        //io_service.run();
    }
    catch (std::exception &e) {
        std::cerr << "Error starting sensor: " << e.what() << std::endl;
    }
}

void ars_408::CanDataReceived(peakData PeakData) {

//    cout << endl << hex //<< "CAN Data: "
//            << (uint) PeakData.get_CanData()[0]
//            << (uint) PeakData.get_CanData()[1]
//            << (uint) PeakData.get_CanData()[2]
//            << (uint) PeakData.get_CanData()[3]
//            << (uint) PeakData.get_CanData()[4]
//            << (uint) PeakData.get_CanData()[5]
//            << (uint) PeakData.get_CanData()[6]
//            << (uint) PeakData.get_CanData()[7]
//            << endl << "CAN ID:   " << (uint)PeakData.get_CanId()
//            << endl << "CAN RTR:  " << (uint)PeakData.get_Rtr()
//            << endl << "Extended: " << (uint)PeakData.get_Extended()
//            << endl << "CAN DLC:  " << (uint)PeakData.get_DLC()
//            << endl << "CAN Type: " << (uint)PeakData.get_MessageType()
//            << endl;

    switch (PeakData.get_CanId()) {
        case 0x60A:
            copyCanFrame(&object_0_Status, PeakData);
            obj1Counter = 0;
            obj2Counter = 0;
            obj3Counter = 0;
            break;
        case 0x60B:
            copyCanFrame(&object_1_General[obj1Counter++], PeakData);
            if (obj1Counter == (object_0_Status.object_NofObjects + 1)) {
                obj1Counter = 0;
            }
            break;
        case 0x60C:
            qualitySent = true;
            copyCanFrame(&object_2_Quality[obj2Counter++], PeakData);
            if(obj2Counter == (object_0_Status.object_NofObjects + 1)) {
                obj2Counter = 0;
            }
            break;
        case 0x60D:
            extendedSent = true;
            copyCanFrame(&object_3_Extended[obj3Counter++], PeakData);
            if(obj3Counter == (object_0_Status.object_NofObjects + 1)) {
                obj3Counter = 0;
            }
            break;
        case 0x3F2:
        	bosch_object_list.num_objects = 0;
        	memcpy(&bosch_object_list.starter, &PeakData, size_t(8));
        	break;
        case 0x3F3:
        case 0x3F5:
        case 0x3F7:
        case 0x3F9:
        case 0x3FB:
        case 0x3FD:
        case 0x3FF:
        case 0x401:
        case 0x403:
        case 0x405:
        case 0x407:
        case 0x409:
        case 0x40B:
        case 0x40D:
        case 0x40F:
        case 0x411:
        case 0x413:
        case 0x415:
        case 0x417:
        case 0x419:
        case 0x41B:
        case 0x41D:
        case 0x41F:
        case 0x421:
        case 0x423:
        case 0x425:
        case 0x427:
        case 0x429:
        case 0x42B:
        case 0x42D:
        case 0x42F:
        case 0x431:
           	memcpy(&bosch_object_list.objectA[bosch_object_list.num_objects], &PeakData, size_t(8));
        	bosch_object_list.num_objects++;
        	break;
        case 0x3F4:
        case 0x3F6:
        case 0x3F8:
        case 0x3FA:
        case 0x3FC:
        case 0x3FE:
        case 0x3F0:
        case 0x402:
        case 0x404:
        case 0x406:
        case 0x408:
        case 0x40A:
        case 0x40C:
        case 0x40E:
        case 0x410:
        case 0x412:
        case 0x414:
        case 0x416:
        case 0x418:
        case 0x41A:
        case 0x41C:
        case 0x41E:
        case 0x420:
        case 0x422:
        case 0x424:
        case 0x426:
        case 0x428:
        case 0x42A:
        case 0x42C:
        case 0x42E:
        case 0x430:
        case 0x432:
        	memcpy(&bosch_object_list.objectB[bosch_object_list.num_objects], &PeakData, size_t(8));
        	break;
        case 0x443:
        	memcpy(&bosch_object_list.ender, &PeakData, size_t(8));
        	sendRosBoschMsg();
        	break;
        default:
            //TODO: Error
            ;
    }

    if(obj3Counter == object_0_Status.object_NofObjects){
        sendRosContiMsg();
    }
}

void ars_408::copyCanFrame(void *destination, peakData PeakData) {
    uint64_t swapHelper(Swap8Bytes(*((uint64_t*)PeakData.get_CanData())));
    memcpy(destination, &swapHelper, size_t(8));
}

void ars_408::sendRosContiMsg(){
    ots_ars408::ars408_ObjectList msgToSend;
    ots_ars408::ars408_Object objectToFill;

    msgToSend.sensor_id = 0;
    for(int i = 0; i < object_0_Status.object_NofObjects; i++){
        objectToFill.id = (uint8_t)object_1_General[i].object_ID;
        objectToFill.prob = probability[object_2_Quality[i].object_ProbOfExist];
        objectToFill.pos_x = object_1_General[i].object_DistLong * (float)0.2 - 500;
        objectToFill.pos_y = object_1_General[i].object_DistLat * (float)0.2 - 204.6;
        objectToFill.vel_x = object_1_General[i].object_VrelLong * (float)0.25 - 128;
        objectToFill.vel_y = object_1_General[i].object_VrelLat * (float)0.25 - 64;
        objectToFill.len = object_3_Extended[i].object_Length * (float)0.2;
        objectToFill.wid = object_3_Extended[i].object_Width * (float)0.2;
        objectToFill.sigma_pos_x = qualityval[object_2_Quality[i].object_DistLong_rms];
        objectToFill.sigma_pos_y = qualityval[object_2_Quality[i].object_DistLat_rms];
        objectToFill.sigma_vel_x = qualityval[object_2_Quality[i].object_VrelLong_rms];
        objectToFill.sigma_vel_y = qualityval[object_2_Quality[i].object_VrelLat_rms];
        objectToFill.sigma_len = 0;
        objectToFill.sigma_wid = 0;
        objectToFill.type = (uint8_t)object_3_Extended[i].object_Class;
        msgToSend.objects.push_back(objectToFill);
    }
    msgToSend.header.stamp = ros::Time::now();
    ars_publisher.publish(msgToSend);
    //ros::spinOnce();
}

void ars_408::sendRosBoschMsg(){
	ots_ars408::boschObjectList msgToSend;
	ots_ars408::boschObject objectToFill;

	msgToSend.num_objects = bosch_object_list.num_objects;
	msgToSend.Veh_psiDt = bosch_object_list.starter.Veh_psiDt * (float)0.0001;
	msgToSend.Veh_kapCurvTraj = bosch_object_list.starter.Veh_kapCurvTraj * (float)0.00001 -0.32;
	msgToSend.Veh_vEgo = bosch_object_list.starter.Veh_vEgo * (float)0.0625 -128;

	for(int i = 0; i < bosch_object_list.num_objects; i++){
		objectToFill.id = (uint8_t)i;
		objectToFill.vr = bosch_object_list.objectA[i].vr * (float)0.0625 - 128;
		objectToFill.dr = bosch_object_list.objectA[i].dr * (float)0.0625;
		objectToFill.probExist = bosch_object_list.objectA[i].wExist * (float)0.03125;
		objectToFill.countAlive = (uint8_t)bosch_object_list.objectA[i].countAlive;
		objectToFill.dbPower = bosch_object_list.objectA[i].dbPower * (float)0.5 - 64;
		objectToFill.sigmaPhi = bosch_object_list.objectA[i].phiSdv * (float)0.001;
		objectToFill.phi = bosch_object_list.objectA[i].phi * (float)0.0002 - 1.6384;
		objectToFill.histFlag = (uint8_t)bosch_object_list.objectA[i].flagHist;
		objectToFill.measurementFlag = (uint8_t)bosch_object_list.objectA[i].flagMeas;
		objectToFill.validFlag = (uint8_t)bosch_object_list.objectA[i].flagValid;
		objectToFill.consistantFlag = (uint8_t)bosch_object_list.objectA[i].MessAconsistBit;
		objectToFill.movingState = (uint8_t)bosch_object_list.objectB[i].movingState;
		objectToFill.vxyValid = (uint8_t)bosch_object_list.objectB[i].vxvyValid;
		objectToFill.vx = bosch_object_list.objectB[i].vxGround * (float)0.0625 - 64;
		objectToFill.vy = bosch_object_list.objectB[i].vyGround * (float)0.0625 - 64;
		objectToFill.vertAngle = bosch_object_list.objectB[i].vertAngle * (float)0.001 - 0.128;
		msgToSend.objects.push_back(objectToFill);
	}

	msgToSend.timeSinceMeas = bosch_object_list.ender.timeSinceMeas;
	msgToSend.vertMisAngle = bosch_object_list.ender.VerticalMisalignmentAngle * (float)0.001 - 0.128;
	msgToSend.horiMisAngle = bosch_object_list.ender.HorizontMisalignmentAngle * (float)0.0001 - 0.2048;
	
	msgToSend.header.stamp = ros::Time::now();
	ars_publisher.publish(msgToSend);
}

void ars_408::config_radar() {
    int helper = 0;
    memcpy(&ars408_cfg0x200, &helper, 8);
    cfg_handler.param("RadarCfg_MaxDistance_valid", helper, 1);
    ars408_cfg0x200.MaxDistance_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SensorID_valid", helper, 1);
    ars408_cfg0x200.SensorID_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_RadarPower_valid", helper, 1);
    ars408_cfg0x200.RadarPower_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_OutputType_valid", helper, 1);
    ars408_cfg0x200.OutputType_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SendQuality_valid", helper, 1);
    ars408_cfg0x200.SendQuality_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SendExtInfo_valid", helper, 1);
    ars408_cfg0x200.SendExtInfo_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SortIndex_valid", helper, 1);
    ars408_cfg0x200.SortIndex_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_StoreInNVM_valid", helper, 1);
    ars408_cfg0x200.StoreInNVM_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_MaxDistance", helper, 200);
    ars408_cfg0x200.MaxDistance = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SensorID", helper, 0);
    ars408_cfg0x200.SensorID = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_OutputType", helper, 1);
    ars408_cfg0x200.OutputType = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_RadarPower", helper, 0);
    ars408_cfg0x200.RadarPower = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_CtrlRelay_valid", helper, 1);
    ars408_cfg0x200.CtrlRelay_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_CtrlRelay", helper, 1);
    ars408_cfg0x200.CtrlRelay = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SendQuality", helper, 1);
    ars408_cfg0x200.SendQuality = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SendExtInfo", helper, 1);
    ars408_cfg0x200.SendExtInfo = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_SortIndex", helper, 0);
    ars408_cfg0x200.SortIndex = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_StoreInNVM", helper, 0);
    ars408_cfg0x200.StoreInNVM = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_RCS_Threshold_valid", helper, 1);
    ars408_cfg0x200.Threshold_valid = static_cast<uint16_t>(helper);
    cfg_handler.param("RadarCfg_RCS_Threshold", helper, 0);
    ars408_cfg0x200.Trheshold = static_cast<uint16_t>(helper);
}

void ars_408::ros_subscribe_callback(ots_ars408::ars408_CarInfo msg){
    uint64_t swapHelper;
    void *sendMsg;
    ars408_SpeedInfo_t ars408_speedInfo;
    ars408_YawRate_t ars408_yawRate;

    ars408_speedInfo.speed_direction = (uint16_t)msg.speed_direction;
    ars408_speedInfo.speed = ((uint16_t )((double)msg.speed)/0.02);

    ars408_yawRate.yawrate = ((uint16_t)((double)msg.yawrate)/0.01) + 0x7FFF;

    memcpy(&swapHelper, &ars408_speedInfo, sizeof(ars408_speedInfo));
    sendMsg = (void*)Swap8Bytes(swapHelper);
    server.peakTx(sendMsg, 2, 0x300);

    memcpy(&swapHelper, &ars408_yawRate, sizeof(ars408_yawRate));
    sendMsg = (void*)Swap8Bytes(swapHelper);
    server.peakTx(sendMsg, 2, 0x301);
}

void ars_408::rosSubscriber() {
    //cout << "test ros subscriber";


    ros::NodeHandle rx_node_handler;
    ros::Subscriber ars_subscriber = rx_node_handler.subscribe("ars408_CarInfo", 32, &ars_408::ros_subscribe_callback, this);

    ros::spin();
    t.detach();
}

ars_408::~ars_408() {

    t.detach();


}



/*        case 0x3F3:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x3F4:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x3F5:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x3F6:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x3F7:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x3F8:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x3F9:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x3FA:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x3FB:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x3FC:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x3FD:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x3FE:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x3FF:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x400:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x401:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x402:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x403:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x404:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x405:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x406:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x407:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x408:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x409:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x40A:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x40B:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x40C:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x40D:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x40E:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x40F:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x410:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x411:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x412:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x413:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x414:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x415:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x416:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x417:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x418:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x419:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x41A:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x41B:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x41C:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x41D:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x41E:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x41F:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x420:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x421:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x422:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x423:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x424:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x425:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x426:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x427:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x428:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x429:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x42A:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x42B:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x42C:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x42D:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x42E:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x42F:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x430:
        	memcpy(&bosch_object00b, PeakData);
        	break;
        case 0x431:
        	memcpy(&bosch_object00a, PeakData);
        	bosch_object_count++;
        	break;
        case 0x432:
        	memcpy(&bosch_object00b, PeakData);
        	break; */


/* SendRosContiMsg:
//#ifdef DEBUG
//    cout << "   Object_0_Status: " << endl << hex
//         << "   NofObjects:  " << object_0_Status.object_NofObjects << endl
//         << "   MeasCounter: " << object_0_Status.object_MeasCounter << endl
//         << "   InterfVersi: " << object_0_Status.object_interfaceVersion << endl
//         << "   Reserved:    " << object_0_Status.reserved1 << endl
//         << "   Reserved2:   " << object_0_Status.reserved2 << endl
//         << "   Reserved3:   " << object_0_Status.reserved3 << endl;
//
//    for(int i = 0; i < object_0_Status.object_NofObjects; i++) {
//        cout << "       1 General [" << i  << "]: " << endl << hex
//             << "           ObjID:          " << object_1_General[i].object_ID << endl
//             << "           Obj_DistLong:   " << object_1_General[i].object_DistLong << endl
//             << "           Obj_DistLat:    " << object_1_General[i].object_DistLat << endl
//             << "           Obj_VrelLong:   " << object_1_General[i].object_VrelLong << endl
//             << "           Obj_VrelLat:    " << object_1_General[i].object_VrelLat << endl
//             << "           Obj_DynProp:    " << object_1_General[i].object_DynProp << endl
//             << "           Obj_RCS:        " << object_1_General[i].object_RCS << endl;
//        cout << "       2 Quality [" << i << "]: " << endl << hex
//             << "           ObjID:           " << object_2_Quality[i].object_ID << endl
//             << "           Obj_DistLong:    " << object_2_Quality[i].object_DistLong_rms << endl
//             << "           Obj_DistLat:     " << object_2_Quality[i].object_DistLat_rms << endl
//             << "           Obj_VrelLong:    " << object_2_Quality[i].object_VrelLong_rms << endl
//             << "           Obj_VrelLat:     " << object_2_Quality[i].object_VrelLat_rms << endl
//             << "           Obj_ArelLong:    " << object_2_Quality[i].object_ArelLong_rms << endl
//             << "           Obj_ArelLat:     " << object_2_Quality[i].object_ArelLat_rms << endl
//             << "           Obj_Orientation: " << object_2_Quality[i].object_Orientation_rms << endl
//             << "           Obj_ProbOfExist: " << object_2_Quality[i].object_ProbOfExist << endl
//             << "           Obj_MeasStat:    " << object_2_Quality[i].object_MeasState << endl;
//        cout << "       3 Extended [" << i << "]:" << endl << hex
//             << "           ObjID:           " << object_3_Extended[i].object_ID << endl
//             << "           Obj_ArelLong:    " << object_3_Extended[i].object_ArelLong << endl
//             << "           Obj_ArelLat:     " << object_3_Extended[i].object_ArelLat << endl
//             << "           Obj_Class:       " << object_3_Extended[i].object_Class << endl
//             << "           Obj_Orientation: " << object_3_Extended[i].object_OrientationAngle << endl
//             << "           Obj_Length:      " << object_3_Extended[i].object_Length << endl
//             << "           Obj_Width:       " << object_3_Extended[i].object_Width << endl;
//    }
//#endif //DEBUG
*/