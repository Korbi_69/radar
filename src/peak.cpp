//
// Created by hsa on 1/20/18.
//

#include "peak.hpp"

    peak::peak(boost::asio::io_service& io_service, std::function<void(peakData)> Callback, uint32_t udpRxPort, uint32_t udpTxProt)
            : socket_(io_service, udp::endpoint(udp::v4(), udpRxPort)), udpRxPort_(udpRxPort), udpTxPort_(udpTxProt)
    {
        dataCallback_= Callback;
        start_receive();
    }

    void peak::start_receive()
    {
        socket_.async_receive_from(
                boost::asio::buffer(recv_buffer_,size_t(36)), remote_endpoint_,
                boost::bind(&peak::handle_receive, this,
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));
    }

    void peak::handle_receive(const boost::system::error_code& error,
                        std::size_t bytes_recvd/*bytes_transferred*/)
    {
        if (!error || error == boost::asio::error::message_size) {
            peakData peakData1(recv_buffer_);

//            can_data canMsg;
//            memcpy(canMsg.data,peakData1.get_CanData(),size_t(8));

            dataCallback_(peakData1);
            start_receive();
        }
    }

    std::string peak::make_daytime_string() {
        using namespace std; // For time_t, time and ctime;
        time_t now = time(nullptr);
        return ctime(&now);
    }

void peak::peakTx(void *canMsg, int dlcCounter, uint32_t canID) {
    char dataToSend[36];
    dataToSend[0]  = (char)0x00;                                // Length
    dataToSend[1]  = (char)0x24;                                // Length
    dataToSend[2]  = (char)0x00;                                // Msg Type
    dataToSend[3]  = (char)0x80;                                // Msg Type
    for(int i = 4; i < 21; i++) {dataToSend[i] = (char)0x00;}   // Tag, Timestamp H/L, Channel
    dataToSend[21] = (char)dlcCounter;                          // DLC
    dataToSend[22] = (char)0x00;                                // Flags
    dataToSend[23] = (char)0x00;                                // Flags
    dataToSend[24] = (char)0x00;
    dataToSend[24] = (char)((canID >> 3) & 0x1F);
    dataToSend[25] = (char)((canID >> 2) & 0xFF);
    dataToSend[26] = (char)((canID >> 1) & 0xFF);
    dataToSend[27] = (char)((canID)      & 0xFF);
    canID = htonl(canID);
    memcpy(&dataToSend[24], &canID, 4);
    memcpy(&dataToSend[28], &canMsg, 8);

    boost::asio::ip::udp::endpoint destination(
            boost::asio::ip::address::from_string("192.168.140.68"), 50001);
    socket_.async_send_to(boost::asio::buffer(dataToSend, 36), destination,
                          boost::bind(&peak::txHandler, this,
                                      boost::asio::placeholders::error,
                                      boost::asio::placeholders::bytes_transferred));
}

void peak::txHandler(const boost::system::error_code &error, std::size_t bytes_transferred) {

}


