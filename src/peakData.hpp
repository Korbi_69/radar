//
// Created by hsa on 1/21/18.
//

#ifndef RADAR_PEAKDATA_HPP
#define RADAR_PEAKDATA_HPP


#include <stdint-gcc.h>
#include <cstring>

class peakData {
public:
    peakData(char Data[]);
    const char *get_CanData() const;
    uint32_t get_CanId() const;
    uint16_t get_Length() const;
    uint16_t get_MessageType() const;
    uint16_t get_Tag() const;
    uint32_t get_TimestampLow() const;
    uint32_t get_TimestampHigh() const;
    uint8_t get_Channel() const;
    uint8_t get_DLC() const;
    uint16_t get_Flags() const;
    const bool get_Rtr();
    const bool get_Extended();

    void set_Length(uint16_t _Length);
    void set_MessageType(uint16_t _MessageType);
    void set_Tag(uint16_t _Tag);
    void set_TimestampLow(uint32_t _TimestampLow);
    void set_TimestampHigh(uint32_t _TimestampHigh);
    void set_Channel(uint8_t _Channel);
    void set_DLC(uint8_t _DLC);
    void set_Flags(uint16_t _Flags);
    void set_CanId(uint32_t _CanId);
    void set_CanData(char* _CanData, int counter);

private:
    uint16_t _Length;
    uint16_t _MessageType;
    uint16_t _Tag;
    uint32_t _TimestampLow;
    uint32_t _TimestampHigh;
    uint8_t _Channel;
    uint8_t _DLC;
    uint16_t _Flags;
    uint32_t _CanId;

    char _CanData[8];


};


#endif //RADAR_PEAKDATA_HPP
