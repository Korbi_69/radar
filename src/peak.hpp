//
// Created by hsa on 1/20/18.
//

#ifndef RADAR_PEAK_H
#define RADAR_PEAK_H

#include <ctime>
#include <iostream>
#include <string>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "can_data.hpp"
#include "peakData.hpp"


using boost::asio::ip::udp;
using namespace std;




class peak {
private:
    udp::socket socket_;
    udp::endpoint remote_endpoint_;
    std::function<void(peakData)> dataCallback_;
    char recv_buffer_[36]={0};

    uint32_t udpRxPort_;
    uint32_t udpTxPort_;


    void start_receive();
    void handle_receive(const boost::system::error_code& error,
                   std::size_t bytes_recvd/*bytes_transferred*/);

    bool checkData();
    std::string make_daytime_string();

    void txHandler(const boost::system::error_code& error,  // Result of operation.
                   std::size_t bytes_transferred);          // Number of bytes sent.

public:
    peak(boost::asio::io_service& io_service, std::function<void(peakData)> dataCallback, uint32_t udpRxPort, uint32_t udpTxProt);
    void peakTx(void *canmsg, int dlcCounter, uint canID);


};


#endif //RADAR_PEAK_H
