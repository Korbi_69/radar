//
// Created by hsa on 1/21/18.
//

#include <netinet/in.h>
#include "peakData.hpp"

peakData::peakData(char Data[]) {
    memcpy(&_Length,&Data[0],size_t(2));
    memcpy(&_MessageType,&Data[2],size_t(2));
    memcpy(&_Tag,&Data[4],size_t(8));
    memcpy(&_TimestampLow,&Data[12],size_t(4));
    memcpy(&_TimestampHigh,&Data[16],size_t(4));
    memcpy(&_Channel,&Data[20],size_t(1));
    memcpy(&_DLC,&Data[21],size_t(1));
    memcpy(&_Flags,&Data[22],size_t(2));
    memcpy(&_CanId,&Data[24],size_t(4));
    memcpy(_CanData,&Data[28],size_t(8));
}

const char *peakData::get_CanData() const {
    return _CanData;
}

uint32_t peakData::get_CanId() const {
    return  (htonl(_CanId))&(0x1FFFFFFF);
}

uint16_t peakData::get_Length() const {
    return htons(_Length);
}

uint16_t peakData::get_MessageType() const {
    return htons(_MessageType);
}

uint16_t peakData::get_Tag() const {
    return _Tag;
}

uint32_t peakData::get_TimestampLow() const {
    return htonl(_TimestampLow);
}

uint32_t peakData::get_TimestampHigh() const {
    return htonl(_TimestampHigh);
}

uint8_t peakData::get_Channel() const {
    return _Channel;
}

uint8_t peakData::get_DLC() const {
    return _DLC;
}

uint16_t peakData::get_Flags() const {
    return _Flags;
}

const bool peakData::get_Rtr() {
    return (_CanId>>6)&1;
}

const bool peakData::get_Extended() {
    return (_CanId>>7)&1;
}

void peakData::set_Length(uint16_t _Length) {
    peakData::_Length = _Length;
}

void peakData::set_MessageType(uint16_t _MessageType) {
    peakData::_MessageType = _MessageType;
}

void peakData::set_Tag(uint16_t _Tag) {
    peakData::_Tag = _Tag;
}

void peakData::set_TimestampLow(uint32_t _TimestampLow) {
    peakData::_TimestampLow = _TimestampLow;
}

void peakData::set_TimestampHigh(uint32_t _TimestampHigh) {
    peakData::_TimestampHigh = _TimestampHigh;
}

void peakData::set_Channel(uint8_t _Channel) {
    peakData::_Channel = _Channel;
}

void peakData::set_DLC(uint8_t _DLC) {
    peakData::_DLC = _DLC;
}

void peakData::set_Flags(uint16_t _Flags) {
    peakData::_Flags = _Flags;
}

void peakData::set_CanId(uint32_t _CanId) {
    peakData::_CanId = _CanId;
}

void peakData::set_CanData(char* _CanData, int counter) {
    for(int i = 0; i < counter; i++) {
        peakData::_CanData[i] = _CanData[i];
    }
}