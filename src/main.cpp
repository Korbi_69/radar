#include "ars_408.hpp"
//#include <ros/ros.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "ars408_publisher");
    ros::NodeHandle tx_node_handler;        // Nachrichten von Radar nach ROS (objektliste)
    //ros::NodeHandle rx_node_handler;        // von ROS nach Radar (v, psi_dot)

    ars_408 sensor1(0, tx_node_handler);
    while (ros::ok())
    {
        ros::spinOnce();
    }

    return 0;

}
