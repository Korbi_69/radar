//
// Created by hsa on 1/20/18.
//

#ifndef RADAR_ARS_408_HPP
#define RADAR_ARS_408_HPP

#define DEBUG

#define RadarRxPort 50000
#define RadarTxPort 50001

#include "peak.hpp"
#include <ctime>
#include <iostream>
#include <string>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/String.h>
#include <ots_ars408/ars408_Object.h>
#include <ots_ars408/ars408_ObjectList.h>
#include <ots_ars408/ars408_CarInfo.h>
#include <ots_ars408/boschObject.h>
#include <ots_ars408/boschObjectList.h>


using boost::asio::ip::udp;
using namespace std;
//using namespace std::placeholders;

#define Swap8Bytes(val) \
    ((((val) >> 56) & 0x00000000000000FF) | (((val) >> 40) & 0x000000000000FF00) | \
     (((val) >> 24) & 0x0000000000FF0000) | (((val) >>  8) & 0x00000000FF000000) | \
     (((val) <<  8) & 0x000000FF00000000) | (((val) << 24) & 0x0000FF0000000000) | \
     (((val) << 40) & 0x00FF000000000000) | (((val) << 56) & 0xFF00000000000000) )



class ars_408 {
private:
    uint8_t sensorId;
    uint16_t bla;
    boost::thread t;
    //std::unique_ptr<peak> server;

    boost::asio::io_service io_service;
    peak server;
    void CanDataReceived(peakData PeakData);
    struct object_0_Status_t{
        uint16_t reserved3 : 16;
        uint16_t reserved2 : 16;
        uint16_t reserved1 : 4;
        uint16_t object_interfaceVersion : 4;
        uint16_t object_MeasCounter : 16;
        uint16_t object_NofObjects : 8;
    }__attribute__ ((packed)) object_0_Status;

    struct object_1_General_t{
        uint16_t object_RCS : 8;
        uint16_t object_DynProp : 3;
        uint16_t reserved : 2;
        uint16_t object_VrelLat : 9;
        uint16_t object_VrelLong : 10;
        uint16_t object_DistLat : 11;
        uint16_t object_DistLong : 13;
        uint16_t object_ID : 8;
    }__attribute__ ((packed)) object_1_General[255];

    struct object_2_Quality_t{
        uint16_t reserved2 : 10;
        uint16_t object_MeasState : 3;
        uint16_t object_ProbOfExist : 3;
        uint16_t reserved1 : 5;
        uint16_t object_Orientation_rms : 5;
        uint16_t object_ArelLat_rms : 5;
        uint16_t object_ArelLong_rms : 5;
        uint16_t object_VrelLat_rms : 5;
        uint16_t object_VrelLong_rms :5;
        uint16_t object_DistLat_rms : 5;
        uint16_t object_DistLong_rms : 5;
        uint16_t object_ID : 8;
    }__attribute__ ((packed)) object_2_Quality[255];

    struct object_3_Extended_t{
        uint16_t object_Width : 8;
        uint16_t object_Length : 8;
        uint16_t reserved2 : 6;
        uint16_t object_OrientationAngle : 10;
        uint16_t object_Class : 3;
        uint16_t reserved1 : 1;
        uint16_t object_ArelLat : 9;
        uint16_t object_ArelLong : 11;
        uint16_t object_ID : 8;
    }__attribute__ ((packed)) object_3_Extended[255];

    struct ars408_Object_t{
        uint8_t id;
        float prob;
        float pos_x;
        float pos_y;
        float vel_x;
        float vel_y;
        float len;
        float wid;
        float sigma_pos_x;
        float sigma_pos_y;
        float sigma_vel_x;
        float sigma_vel_y;
        float sigma_len;
        float sigma_wid;
        uint8_t type;
    }ars408_object[255];

    struct ars408_Object_Raw_t{
        uint16_t id             : 8;
        uint16_t prob           : 3;
        uint16_t pos_x          : 13;
        uint16_t pos_y          : 11;
        uint16_t vel_x          : 10;
        uint16_t vel_y          : 9;
        uint16_t len            : 8;
        uint16_t wid            : 8;
        uint16_t sigma_pos_x    : 5;
        uint16_t sigma_pos_y    : 5;
        uint16_t sigma_vel_x    : 5;
        uint16_t sigma_vel_y    : 5;
        uint16_t sigma_len      : 5;
        uint16_t sigma_wid      : 5;
        uint16_t type           : 3;
    }ars408_object_raw[255];

    struct ars408_SpeedInfo_t{
        uint16_t speed           : 13;
        uint16_t reserved        : 1;
        uint16_t speed_direction : 2;
    }__attribute__ ((packed));// ars408_speedInfo;

    struct ars408_YawRate_t{
        uint16_t yawrate         : 16;
    }__attribute__ ((packed));// ars408_yawRate;

    struct ars408_Cfg0x200_t{
        uint16_t reserved1              : 8;
        uint16_t Threshold_valid        : 1;
        uint16_t Trheshold              : 3;
        uint16_t reserved2              : 4;
        uint16_t CtrlRelay_valid        : 1;
        uint16_t CtrlRelay              : 1;
        uint16_t SendQuality            : 1;
        uint16_t SendExtInfo            : 3;
        uint16_t SortIndex              : 1;
        uint16_t StoreInNVM             : 1;
        uint16_t SensorID               : 3;
        uint16_t OutputType             : 2;
        uint16_t RadarPower             : 3;
        uint16_t reserved3              : 14;
        uint16_t MaxDistance            : 10;
        uint16_t MaxDistance_valid      : 1;
        uint16_t SensorID_valid         : 1;
        uint16_t RadarPower_valid       : 1;
        uint16_t OutputType_valid       : 1;
        uint16_t SendQuality_valid      : 1;
        uint16_t SendExtInfo_valid      : 1;
        uint16_t SortIndex_valid        : 1;
        uint16_t StoreInNVM_valid       : 1;
    }__attribute__ ((packed)) ars408_cfg0x200;

    int bosch_object_count;

    struct bosch_object_starter_t{
        uint16_t Veh_psiDt              : 16;
        uint16_t Counter                : 16;
        uint16_t Veh_kapCurvTraj        : 16;
        uint16_t Veh_vEgo               : 12;
        uint16_t reserved               : 3;
        uint16_t MessStarterConsistBit  : 1;
    }__attribute__ ((packed)) bosch_object_starter;

    struct bosch_objectA_t{
        uint16_t vr                     : 12;
        uint16_t dr                     : 12;
        uint16_t wExist                 : 5;
        uint16_t countAlive             : 3;
        uint16_t dbPower                : 8;
        uint16_t phiSdv                 : 6;
        uint16_t phi                    : 14;
        uint16_t flagHist               : 1;
        uint16_t flagMeas               : 1;
        uint16_t flagValid              : 1;
        uint16_t MessAconsistBit        : 1;
    }__attribute__ ((packed)) bosch_objectA;

    struct bosch_objectB_t{
        uint16_t MessBconsistBit        : 1;
        uint16_t movingState            : 3;
        uint16_t vxvyValid              : 1;
        uint16_t vxGround               : 11;
        uint16_t vyGround               : 11;
        uint16_t reserved1              : 5;
        uint16_t vertAngle              : 8;
        uint16_t reserved2              : 8;
        uint16_t reserved3              : 16;
    }__attribute__ ((packed)) bosch_objectB;

    struct bosch_object_ender_t{
        uint16_t timeSinceMeas          : 13;
        uint16_t reserved               : 3;
        uint16_t MountAngle             : 15;
        uint16_t MeasEnderConsistBit    : 1;
        uint16_t VerticalMisalignmentAngle : 8;
        uint16_t HorizontMisalignmentAngle : 12;
        uint16_t PacketMessageCounter   : 4;
        uint16_t PacketChecksum         : 8;
    }__attribute__ ((packed)) bosch_object_ender;

    struct bosch_object_list_t{
        uint16_t num_objects;
        bosch_object_starter_t starter;
        bosch_objectA_t objectA[32];
        bosch_objectB_t objectB[32];
        bosch_object_ender_t ender;
    }__attribute__ ((packed)) bosch_object_list;

    
    struct bosch_RAD_INPUT_CONFDATA_t{
        uint16_t RI_sensorMountAngle    : 15;
        uint16_t RI_LongSensorMountToRearAxle : 9;
        uint16_t RI_LatSensorMountToCenter : 7;
        uint16_t RI_SensorOrientation   : 1;
        uint16_t RI_Veh_WheelBase       : 8;
        uint16_t RI_Veh_Steer_angle_ratio : 8;
        uint16_t RI_DeActivateObjectMessage : 1;
        uint16_t RI_ActivateFunctionMessage : 1;
        uint16_t RI_SendBMessages       : 1;
        uint16_t RI_Conf_MC             : 4;
        uint16_t RI_Conf_CRC            : 8;
    }__attribute__ ((packed)) bosch_RAD_INPUT_CONFDATA;

    struct bosch_RAD_INPUT_VEH_DYNDATA_t{
        uint16_t RI_Veh_Steer_angle     : 16;
        uint16_t RI_Veh_velocity        : 16;
        uint16_t RI_Veh_yawRate         : 14;
        uint16_t RI_Veh_Use_Steer_angle : 1;
        uint16_t RI_Veh_stillstand      : 1;
        uint16_t RI_Veh_AnyWheelSlipEvent : 1;
        uint16_t RI_Veh_MC              : 4;
        uint16_t RI_Veh_CRC             : 8;
    }__attribute__ ((packed)) bosch_RAD_INPUT_VEH_DYNDATA;

    struct bosch_RAD_INPUT2_t{
        uint16_t RI2_Veh_TL             : 7;
        uint16_t RI2_externalFault      : 1;
        uint16_t RI2_sensorMountingHeight : 15;
        uint16_t RI2_wiperStatus        : 1;
        uint16_t RI2_indicatorStatus    : 2;
        uint16_t RI2_LCAenabled         : 1;
        uint16_t RI2_Veh_MC             : 4;
        uint16_t RI2_Veh_CRC            : 8;
        uint16_t reserved1              : 16;
        uint16_t reserved2              : 16;
    }__attribute__ ((packed)) bosch_RAD_INPUT2;

    uint8_t obj1Counter = 0;
    uint8_t obj2Counter = 0;
    uint8_t obj3Counter = 0;

    bool qualitySent = 0;
    bool extendedSent = 0;


    void copyCanFrame(void *destination, peakData PeakData);


    void sendRosContiMsg();
    void sendRosBoschMsg();
    void config_radar();

    ros::NodeHandle cfg_handler;
    ros::Publisher ars_publisher;

    const float probability[8] = {0, 0.25, 0.5, 0.75, 0.9, 0.99, 0.999, 1.0};
    const float qualityval[31] = {0.005, 0.006, 0.008, 0.011, 0.014, 0.018, 0.023, 0.029, 0.038, 0.049,
                                0.063, 0.081, 0.105, 0.135, 0.174, 0.224, 0.288, 0.371, 0.478, 0.616,
                                0.794, 1.317, 1.697, 2.187, 2.817, 3.630, 4.676, 6.025, 7.762, 10.00, 999};

public:
    ars_408(uint8_t sensorId, ros::NodeHandle tx_node_handler);

    //ars_408(const peak &server);
    ~ars_408();

    void rosSubscriber();
    void ros_subscribe_callback(ots_ars408::ars408_CarInfo msg);
};


#endif //RADAR_ARS_408_HPP

/*
    struct bosch_object00a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object00a;

    struct bosch_object00b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object00b;

    struct bosch_object01a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object01a;

    struct bosch_object01b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object01b;

    struct bosch_object02a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object02a;

    struct bosch_object02b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object02b;

    struct bosch_object03a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object03a;

    struct bosch_object03b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object03b;

    struct bosch_object04a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object04a;

    struct bosch_object04b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object04b;

    struct bosch_object05a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object05a;

    struct bosch_object05b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object05b;

    struct bosch_object06a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object06a;

    struct bosch_object06b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object06b;

    struct bosch_object07a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object07a;

    struct bosch_object07b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object07b;

    struct bosch_object08a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object08a;

    struct bosch_object08b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object08b;

    struct bosch_object09a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object09a;

    struct bosch_object09b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object09b;

    struct bosch_object10a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object10a;

    struct bosch_object10b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object10b;

    struct bosch_object11a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object11a;

    struct bosch_object11b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object11b;

    struct bosch_object12a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object12a;

    struct bosch_object12b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object12b;

    struct bosch_object13a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object13a;

    struct bosch_object13b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object13b;

    struct bosch_object14a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object14a;

    struct bosch_object14b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object14b;

    struct bosch_object15a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object15a;

    struct bosch_object15b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object15b;

    struct bosch_object16a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object16a;

    struct bosch_object16b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object16b;

    struct bosch_object17a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object17a;

    struct bosch_object17b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object17b;

    struct bosch_object18a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object18a;

    struct bosch_object18b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object18b;

    struct bosch_object19a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object19a;

    struct bosch_object19b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object19b;

    struct bosch_object20a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object20a;

    struct bosch_object20b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object20b;

    struct bosch_object21a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object21a;

    struct bosch_object21b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object21b;

    struct bosch_object22a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object22a;

    struct bosch_object22b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object22b;

    struct bosch_object23a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object23a;

    struct bosch_object23b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object23b;

    struct bosch_object24a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object24a;

    struct bosch_object24b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object24b;

    struct bosch_object25a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object25a;

    struct bosch_object25b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object25b;

    struct bosch_object26a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object26a;

    struct bosch_object26b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object26b;

    struct bosch_object27a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object27a;

    struct bosch_object27b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object27b;

    struct bosch_object28a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object28a;

    struct bosch_object28b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object28b;

    struct bosch_object29a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object29a;

    struct bosch_object29b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object29b;

    struct bosch_object30a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object30a;

    struct bosch_object30b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object30b;

    struct bosch_object31a_t{
        uint16_t Object00_vr : 12;
        uint16_t Object00_dr : 12;
        uint16_t Object00_wExist : 5;
        uint16_t Object00_countAlive : 3;
        uint16_t Object00_dbPower : 8;
        uint16_t Object00_phiSdv : 6;
        uint16_t Object00_phi : 14;
        uint16_t Object00_flagHist : 1;
        uint16_t Object00_flagMeas : 1;
        uint16_t Object00_flagValid : 1;
        uint16_t Object00_MessAconsistBit : 1;
    }__attribute__ ((packed)) bosch_object31a;

    struct bosch_object31b_t{
        uint16_t Object00_MessBconsistBit : 1;
        uint16_t Object00_movingState : 3;
        uint16_t Object00_vxvyValid : 1;
        uint16_t Object00_vxGround : 11;
        uint16_t Object00_vyGround : 11;
        uint16_t reserved1 : 5;
        uint16_t Object00_vertAngle : 8;
        uint16_t reserved2 : 8;
        uint16_t reserved3 : 16;
    }__attribute__ ((packed)) bosch_object31b;
*/